<?php

use App\Http\Controllers\PajakKendaraan\PajakKendaraanAcehCont;
use App\Http\Controllers\PajakKendaraan\PajakKendaraanRiauCont;
use App\Http\Controllers\PajakKendaraan\PajakKendaraanSumbarCont;
use App\Http\Controllers\PajakKendaraan\PajakKendaraanSumselCont;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'pajak-kendaraan'], function () {
    Route::get('riau', [PajakKendaraanRiauCont::class, 'getPajak']);
    Route::get('aceh', [PajakKendaraanAcehCont::class, 'getPajak']);
    Route::get('sumbar', [PajakKendaraanSumbarCont::class, 'getPajak']);
    Route::get('sumsel', [PajakKendaraanSumselCont::class, 'getPajak']);
});
