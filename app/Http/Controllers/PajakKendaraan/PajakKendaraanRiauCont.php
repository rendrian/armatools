<?php

namespace App\Http\Controllers\PajakKendaraan;

use App\Helperx;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class PajakKendaraanRiauCont extends Controller
{
    public function getPajak($nopol = 'BM6331FP', Request $request)
    {

        $nopol = (isset($request->nopol) ? $request->nopol : $nopol);

        $parsed = (object) Helperx::parsing('bm', $nopol);

        // $param = 'Sewil=' . $parsed->serwil . '&NoPol=' . $parsed->nomor . '&Seri=' . $parsed->seri . '';
        $param = 'Sewil=BM&NoPol=6331&Seri=FP';
        $curl = curl_init();
        $curlHeader = array(
            "Connection: keep-alive",
            "Accept: application/json, text/javascript, */*",
            "X-Requested-With: XMLHttpRequest",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36",
            "Content-Type: application/x-www-form-urlencoded",
            "Origin: https://badanpendapatan.riau.go.id",
            "Sec-Fetch-Site: same-origin",
            "Sec-Fetch-Mode: cors",
            "Sec-Fetch-Dest: empty",
            "Accept-Language: en-US,en;q=0.9",

        );
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://badanpendapatan.riau.go.id/infopajak_master/indexjson.php",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $param,
            CURLOPT_HTTPHEADER => $curlHeader,
        ));

        $response = curl_exec($curl);

        if (curl_errno($curl)) {
            return response([
                'success' => false,
                'reason' => "Server Not Responding",
            ]);
        }

        curl_close($curl);
        $response = json_decode($response, false);
        return response([
            'success' => true,
            'data' => $response,
        ]);

    }
}
