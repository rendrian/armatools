<?php

namespace App\Http\Controllers\PajakKendaraan;

use App\Helperx;
use App\Http\Controllers\Controller;
use DiDom\Document;
use Illuminate\Http\Request;

class PajakKendaraanSumselCont extends Controller
{
    /**
     * e-Samsat Sumatera Selatan
     * * http://bapenda.sumselprov.go.id/index.php/page/bbn
     */
    public $seriWilayah = 'BG';
    public function getPajak($nopol = 'BG6205ACY', Request $request)
    {

        $nopol = (isset($request->nopol) ? $request->nopol : $nopol);

        $parsed = (object) Helperx::parsing('bm', $nopol);

        $param = 'nopol2=' . $parsed->nomor . '&nopol3=' . $parsed->seri . '&tekan=';

        $html = $this->cUrl($param);

        $dom = new Document($html);
        if (!$dom->has('form#form_class')) {
            return Helperx::invalidResponse('Fetch data error contact admin', '#0001');
        }

        // #1 Detecing form#form_class
        $form = $dom->find('form#form_class')[0];
        // #2 get div.table-responsive
        $resultArray = [];
        if ($form->has('table')) {
            foreach ($form->find('table') as $table) {

                foreach ($table->find('input') as $input) {
                    // array_map(function () {

                    // }, $input);
                    array_push($resultArray, [
                        'id' => $this->translate($input->{'id'}),
                        'value' => $input->{'value'},
                    ]);
                }
            }

        }

        $arrayColumn = array_column($resultArray, 'value', 'id');

        return Helperx::validResponse($arrayColumn);

    }

    private function translate($string)
    {
        $transbase = [
            'nama_pemilik' => 'nomor_polisi',
            'alamat2' => 'nilai_jual',
            "bbnkb1" => "bbnkb_pokok",
            "bbnkb2" => "bbnkb_sanksi",
            "bbnkb3" => "bbnkb_jumlah",
            "pkb_tunggakan1" => "pkb_pokok",
            "pkb_tunggakan2" => "pkb_sanksi",
            "pkb_tunggakan3" => "pkb_jumlah",
            "swdkllj1" => "swdkllj_pokok",
            "swdkllj2" => "swdkllj_sanksi",
            "swdkllj3" => "swdkllj_jumlah",
            "adm_stnk1" => "adm_stnk_pokok",
            "adm_stnk2" => "adm_stnk_sanksi",
            "adm_stnk3" => "adm_stnk_jumlah",
            "adm_tnkb1" => "adm_tnkb_pokok",
            "adm_tnkb2" => "adm_tnkb_sanksi",
            "adm_tnkb3" => "adm_tnkb_jumlah",
            "jumlah1" => "total_pokok",
            "jumlah2" => "total_sanksi",
            "jumlah3" => "total_total",
        ];
        if (array_key_exists($string, $transbase)) {
            return $transbase[$string];

        } else {
            return $string;
        }
    }

    private function cUrl($param)
    {

        $header = array(
            "Connection: keep-alive",
            "Cache-Control: max-age=0",
            "Upgrade-Insecure-Requests: 1",
            "Origin: http://bapenda.sumselprov.go.id",
            "Content-Type: application/x-www-form-urlencoded",
            "User-Agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36",
            "Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9",
            "Referer: http://bapenda.sumselprov.go.id/index.php/page/bbn",
            "Accept-Language: en-US,en;q=0.9",

        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "http://bapenda.sumselprov.go.id/index.php/page/bbn",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $param,
            CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        return $response;

    }
}
