<?php

namespace App\Http\Controllers\PajakKendaraan;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class PajakKendaraanSumbarCont extends Controller
{

    public $seriWilayah = 'BA';
    public function getPajak($nopol = 'BA6825OL', Request $request)
    {
        // sample :  BA6825OL

        // $ba = (isset($request->ba) ? $request->ba : $ba);
        $nomorPolisi = (isset($request->nopol) ? $request->nopol : $nopol);

        // $param = "ba=$ba&nomor=$nomor&seri=$seri";
        // $param = 'Sewil=' . $sewil . '&NoPol=' . $nopol . '&Seri=' . $seri . '';

        $parsedData = (object) $this->parsing($nomorPolisi);
        $param = 'ba=' . $parsedData->serwil . '&nomor=' . $parsedData->nomor . '&seri=' . $parsedData->seri;

        $header = array(
            "authority: dpkd.sumbarprov.go.id",
            "accept: application/json, text/javascript, */*; q=0.01",
            "x-requested-with: XMLHttpRequest",
            "user-agent: Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36",
            "content-type: application/x-www-form-urlencoded; charset=UTF-8",
            "origin: https://dpkd.sumbarprov.go.id",
            "sec-fetch-site: same-origin",
            "sec-fetch-mode: cors",
            "sec-fetch-dest: empty",
            "referer: https://dpkd.sumbarprov.go.id/info-pkb.html",
            "accept-language: en-US,en;q=0.9",

        );
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://dpkd.sumbarprov.go.id/ajax/pkb",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $param,
            CURLOPT_HTTPHEADER => $header,
        ));

        $response = curl_exec($curl);

        curl_close($curl);
        $response = json_decode($response, false);
        return response([
            'success' => true,
            'data' => $response->GetDataRanmor,
        ]);
    }

    private function parsing($nopol)
    {
        // $pecah = explode($this->seriWilayah,$nopol);
        $nomor = $this->getNomorPolisi($nopol);
        return [
            'serwil' => $this->seriWilayah,
            'nomor' => $nomor,
            'seri' => explode($nomor, $nopol)[1],
        ];
        // return $this->getNomorPolisi($nopol);

    }

    private function getNomorPolisi($string)
    {
        preg_match_all('!\d+!', $string, $matches);
        return implode('', $matches[0]);

    }
}
