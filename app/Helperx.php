<?php
namespace App;

class Helperx
{
    public static function parsing($seriWilayah, $nopol)
    {
        $nopol = strtolower($nopol);

        $nomor = self::getNomorPolisi($nopol);
        return [
            'serwil' => strtolower($seriWilayah),
            'nomor' => $nomor,
            'seri' => explode($nomor, $nopol)[1],
        ];
        // return $this->getNomorPolisi($nopol);

    }

    public static function getNomorPolisi($string)
    {

        preg_match_all('!\d+!', $string, $matches);
        return implode('', $matches[0]);

    }

    public static function validResponse($response)
    {
        return response([
            'success' => true,
            'data' => $response,
        ], 200);
    }
    public static function invalidResponse($reason,$code)
    {
        return response([
            'success' => false,
            'code'=>$code,
            'reason' => $reason,
        ], 201);
    }
}
